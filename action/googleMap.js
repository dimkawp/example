import store from "../redux/store";
import { mapping } from "./mapping";
import _ from "lodash";
import { api } from "../api/api";
import helpFunctions from "../tools/helpFunctions";
import { filter } from "./filters";

export const googleMap = {
  initGoogleMap(options) {
    store.dispatch({
      type: "GOOGLE_MAP_INIT",
      payload: {
        options: options
      }
    });
  },
  async getMarkersInfoById(id) {
    store.dispatch({
      type: "GOOGLE_MAP_ADVENCED_MARKER_INFO_LOADER",
      payload: {
        advencedMarkerInfoLoader: true
      }
    });
    let response = await api.getAdvertById(id);
    store.dispatch({
      type: "GOOGLE_MAP_ADVENCED_MARKER_INFO",
      payload: {
        advencedMarkerInfo: mapping.getMarkerAdvertInfo(
          response.data.advert,
          store.getState().app.categorysList
        )
      }
    });
    store.dispatch({
      type: "GOOGLE_MAP_ADVENCED_MARKER_INFO_LOADER",
      payload: {
        advencedMarkerInfoLoader: false
      }
    });
  },
  async getMarkersByParams(getBouds) {
    let getNorthEast;
    let getSouthWest;
    if (getBouds) {
      getNorthEast = getBouds.getNorthEast();
      getSouthWest = getBouds.getSouthWest();
    }

    store.dispatch({
      type: "GOOGLE_MAP_LOADER",
      payload: {
        googleMapLoader: true
      }
    });
    let filter = store.getState().filter.filterData;
    if (filter.region.length > 0) {
      let coord = {
        lng: filter.region[0].lng,
        lat: filter.region[0].lat
      };
    }
    let response = await api.getMarkersForGoogleMapByFilters({
      filters: {
        categoryId: filter.categoryId,
        type: filter.type,
        transaction_type: filter.transactionType,
        status: filter.status,
        value: filter.value,
        region: [],
        size: {
          min: filter.minSize,
          max: filter.maxSize
        },
        price: {
          min: filter.minPrice,
          max: filter.maxPrice
        },
        unitPrice: {
          min: filter.minUnitPrice,
          max: filter.maxUnitPrice
        },
        coord: {
          lat: null,
          lng: null
        },
        bounds: {
          north: getNorthEast ? getNorthEast.lat() : null,
          south: getSouthWest ? getSouthWest.lat() : null,
          west: getSouthWest ? getSouthWest.lng() : null,
          east: getNorthEast ? getNorthEast.lng() : null
        }
      }
    });

    let newOb = [];
    let map = response.data.list;
    if (map.length > 0) {
      map.map(item => {
        let filter = _.filter(map, {
          lng: item.lng,
          lat: item.lat
        });
        return newOb.push({
          id: item.id,
          lng: item.lng,
          lat: item.lat,
          array: filter.map(item => {
            return item;
          }),
          activeStatus: false
        });
      });
    }

    store.dispatch({
      type: "GOOGLE_MAP_LOADER",
      payload: {
        googleMapLoader: false
      }
    });
    store.dispatch({
      type: "GOOGLE_MAP_PARAMS",
      payload: {
        markers: newOb
      }
    });
    store.dispatch({
      type: "ADVERT_LIST_PRELOADER",
      payload: {
        preloader: false
      }
    });
  },
  showMapController(value) {
    store.dispatch({
      type: "GOOGLE_MAP_SHOW_CONTROLLER",
      payload: {
        googleMapShowContorller: value
      }
    });
  },
  async getInfoAboutMarkersForMobile(coord, contextFrom) {
    store.dispatch({
      type: "GOOGLE_MAP_LOADER",
      payload: {
        googleMapLoader: true
      }
    });
    store.dispatch({
      type: "GOOGLE_MAP_MAKRERS_INFO_FOR_MOBILE",
      payload: {
        advencedMarkerInfoForMobile: []
      }
    });
    let filter = store.getState().filter.filterData;
    let filters = {};
    if (contextFrom === "catalog") {
      filters = {
        limit: 999,
        filters: {
          categoryId: filter.categoryId,
          type: filter.type,
          transaction_type: filter.transactionType,
          status: filter.status,
          value: filter.value,
          region: filter.region.map(reg => {
            return { place_id: reg.place_id };
          }),
          size: {
            min: filter.minSize,
            max: filter.maxSize
          },
          price: {
            min: filter.minPrice,
            max: filter.maxPrice
          },
          unitPrice: {
            min: filter.minUnitPrice,
            max: filter.maxUnitPrice
          },
          coord: coord
        }
      };
    }
    if (contextFrom === "single") {
      filters = {
        limit: 999,
        filters: {
          coord: coord
        }
      };
    }

    let response = await api.getAdByFilterParams({
      ...filters
    });
    store.dispatch({
      type: "GOOGLE_MAP_MAKRERS_INFO_FOR_MOBILE",
      payload: {
        advencedMarkerInfoForMobile: mapping.getAdList(
          response.data.list.slice(0, 50)
        )
      }
    });
    store.dispatch({
      type: "GOOGLE_MAP_LOADER",
      payload: {
        googleMapLoader: false
      }
    });
  },
  changecenterPosition(coord) {
    store.dispatch({
      type: "GOOGLE_MAP_PARAMS_CENTER_POSITION",
      payload: {
        centerPosition: { lat: coord.lat, lng: coord.lng }
      }
    });
  },
  changeZoomPostion(zoom) {
    store.dispatch({
      type: "GOOGLE_MAP_PARAMS_ZOOM_POSITION",
      payload: {
        zoomPosition: zoom
      }
    });
  }
};
