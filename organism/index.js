import React from "react";
import { Loader } from "semantic-ui-react";
import { compose, withProps, withHandlers } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps";
import { MarkerClusterer } from "react-google-maps/lib/components/addons/MarkerClusterer";
import { MarkerWithLabel } from "react-google-maps/lib/components/addons/MarkerWithLabel";
// store
import store from "../../../redux/store";
// style
import "./style.scss";
const GoogleMapParent = compose(
  withProps({
    googleMapURL: `${
      store.getState().googleMap.options.googleMapURL
    }${`&language=${store.getState().app.language}`}`,
    loadingElement: (
      <div
        className={store.getState().googleMap.options.classNameMapLoading}
      ></div>
    ),
    containerElement: (
      <div
        className={store.getState().googleMap.options.classNameMapContainer}
      />
    ),
    mapElement: (
      <div className={store.getState().googleMap.options.classNameMapElement} />
    )
  }),
  withHandlers({
    onMarkerClustererClick: () => markerClusterer => {
      props.onMarkerClustererClick(props, event);
    }
  }),
  withScriptjs,
  withGoogleMap
)(props => {
  onCenterChanged = () => {
    props.onCenterChanged(this.getCenter(), this.getBounds());
  }
  onZoomChanged = () => {
    props.onZoomChanged(this.getCenter(), this.getBounds());
  }
  return (
    <React.Fragment>
      {props.googleMapLoader && <Loader active className="map-loader" />}
      <GoogleMap
        defaultZoom={props.zoom.defaultZoom}
        defaultCenter={{
          lat: props.defaultMapPosition.lat,
          lng: props.defaultMapPosition.lng
        }}
        center={
          props.selectedMarker && props.selectedMarkerController
            ? { lat: props.selectedMarker.lat, lng: props.selectedMarker.lng }
            : {
                lat: props.centerPosition.lat,
                lng: props.centerPosition.lng
              }
        }
        zoom={props.zoomPosition ? props.zoomPosition : props.zoom.defaultZoom}
        onClick={props.clickOnMap}
        onZoomChanged={onZoomChanged}
        onCenterChanged={onCenterChanged}
      >
        <MarkerClusterer
          maxZoom={props.zoom.maxZoom}
          onClick={props.onMarkerClustererClick}
          averageCenter={true}
          enableRetinaIcons={true}
          styles={props.classterStyles}
          ignoreHidden={true}
        >
          {props.markers ? (
            props.markers.map(marker => {
              const onClick = props.onClickMarker.bind(this, marker);
              const self = props.selectedMarker
                ? props.selectedMarker.id === marker.id
                  ? true
                  : false
                : false;
              const similar = props.selectedMarker
                ? props.selectedMarker.lat === marker.lat &&
                  props.selectedMarker.lng === marker.lng
                  ? true
                  : false
                : false;
              const single = marker.array.length > 1 ? false : true;
              const first = marker.array[0].id === marker.id ? true : false;
              return (
                <MarkerWithLabel
                  key={marker.id}
                  onClick={onClick}
                  position={{ lat: marker.lat, lng: marker.lng }}
                  labelAnchor={{ x: 0, y: 0 }}
                  labelClass={
                    single
                      ? `marker-label ${first ? "first" : ""}`
                      : `marker-label group ${first ? "first" : ""}`
                  }
                  className="claster"
                  icon={{ url: "#" }}
                >
                  {!self ? (
                    <div
                      className={
                        marker.array.length > 9
                          ? `large-marker ${similar ? "similar" : ""}`
                          : `small-marker ${similar ? "similar" : ""}`
                      }
                    >
                      {marker.array.length}
                    </div>
                  ) : (
                    <div
                      className={
                        marker.array.length > 9
                          ? `active large-marker ${similar ? "similar" : ""}`
                          : `active small-marker ${similar ? "similar" : ""}`
                      }
                    >
                      {marker.array.length}
                    </div>
                  )}
                </MarkerWithLabel>
              );
            })
          ) : (
            <React.Fragment></React.Fragment>
          )}
        </MarkerClusterer>
      </GoogleMap>
    </React.Fragment>
  );
});

export default GoogleMapParent;
