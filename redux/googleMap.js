/*
 *
 * GoogleMap reducer
 *
 */
import { CONSTS } from "../../config/objectConst";
const initialState = {
  markers: [],
  defaultMapPosition: {},
  advencedMarkerInfo: false,
  advencedMarkerInfoLoader: false,
  advencedMarkerInfoForMobile: [],
  googleMapShowContorller: false,
  googleMapLoader: false,
  centerPosition: { lat: 55.751244, lng: 37.618423 },
  zoomPosition: 6,
  options: {
    googleMapURL: CONSTS.GOOGLE_MAP_PARAMS.url,
    classNameMapContainer: "map-container",
    classNameMapLoading: "map-container-loading",
    classNameMapElement: "map-container-element"
  }
};

const googleMap = (state = initialState, action) => {
  switch (action.type) {
    case "GOOGLE_MAP_INIT":
      return Object.assign({}, state, {
        options: action.payload.options
      });
    case "GOOGLE_MAP_PARAMS":
      return Object.assign({}, state, {
        markers: action.payload.markers,
        defaultMapPosition: action.payload.defaultMapPosition
      });
    case "GOOGLE_MAP_PARAMS_CENTER_POSITION":
      return Object.assign({}, state, {
        centerPosition: action.payload.centerPosition
      });
    case "GOOGLE_MAP_PARAMS_ZOOM_POSITION":
      return Object.assign({}, state, {
        zoomPosition: action.payload.zoomPosition
      });
    case "GOOGLE_MAP_LOADER":
      return Object.assign({}, state, {
        googleMapLoader: action.payload.googleMapLoader
      });
    case "GOOGLE_MAP_SHOW_CONTROLLER":
      return Object.assign({}, state, {
        googleMapShowContorller: action.payload.googleMapShowContorller
      });
    case "GOOGLE_MAP_ADVENCED_MARKER_INFO":
      return Object.assign({}, state, {
        advencedMarkerInfo: action.payload.advencedMarkerInfo
      });
    case "GOOGLE_MAP_ADVENCED_MARKER_INFO_LOADER":
      return Object.assign({}, state, {
        advencedMarkerInfoLoader: action.payload.advencedMarkerInfoLoader
      });
    case "GOOGLE_MAP_MAKRERS_INFO_FOR_MOBILE":
      return Object.assign({}, state, {
        advencedMarkerInfoForMobile: action.payload.advencedMarkerInfoForMobile
      });

    default:
      return state;
  }
};

export default googleMap;
