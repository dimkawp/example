export const api = {
  getMarkersForGoogleMapByFilters: async data => {
    return await axios({
      url: CONSTS.API.url + "item/map/list",
      method: "POST",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        "user-token": localStorage.getItem("token")
          ? localStorage.getItem("token")
          : null
      },
      data: data
    });
  },
  getMarkersInfoByIds: async data => {
    return await axios({
      url: CONSTS.API.url + `item/map/info?adverts=${data}`,
      method: "GET",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "user-token": localStorage.getItem("token")
          ? localStorage.getItem("token")
          : null
      }
    });
  }
}